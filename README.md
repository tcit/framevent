**Framevent** est un projet pour permettre de recueillir les invitations à des événements de manière *simple* et de les *transmettre* aux membres d'une association facilement.
